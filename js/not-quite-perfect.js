// Not Quite Perfect

// A perfect number is a number that is equal to the sum
// of all its proper (non-self) divisors. Take 6 for example:

// 6 = 1 + 2 + 3

// A number that is equal to the sum of all its proper
// divisors -- provided that one of them is negative --
// is not quite perfect, but admirable.

// To illustrate. The proper divisors of 12 are 1, 2, 3, 4, 6,
// totalling 16. However, if 2 is negative, the total would be 12,
// the number itself. 12 is therefore an admirable number.

// 12 = 1 - 2 + 3 + 4 + 6

// Create a function that takes a number n as input.

// If n is perfect, return "Perfect".
// If n is admirable, return the proper divisor that must be
// rendered negative to make the sum of the proper divisors equal to n.
// If n is neither perfect nor admirable, return "Neither".

// Examples

// admirable(6) ➞ "Perfect"
// admirable(12) ➞ 2
// admirable(18) ➞ "Neither"

const admirable = n => {
  let divisors = [];
  const halfN = n / 2;

  for (let i = 0; i < halfN; i++) {
    if (n % (i + 1) == 0) {
      divisors.push(i + 1);
    }
  }

  const sumOfDivisors = divisors.reduce((a, b) => a + b, 0);

  if (sumOfDivisors == n) {
    return "Perfect";
  } else {
    for (let i = 0; i < divisors.length; i++) {
      const currentNumber = divisors[i];
      const newDivisors = divisors.filter(number => number != currentNumber);
      const sumOfNewDivisors = newDivisors.reduce((a, b) => a + b, 0);

      if (sumOfNewDivisors - currentNumber == n) {
        return currentNumber;
      }
    }
  }

  return "Neither";
};

console.log(admirable(6));
console.log(admirable(12));
console.log(admirable(18));
